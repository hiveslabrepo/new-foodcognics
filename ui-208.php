		<link href="css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Yeseva+One" />
		
		
		<!-- Main CSS -->
		<link href="css/style-208.css" rel="stylesheet">
				
		<!-- Favicon -->
		<link rel="shortcut icon" href="#">
		<style>
		.form-control {
    display: block;
    width: 90%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}
.form-group{

	margin-left: 7%;
}
.row {
    margin-right: 0px;
    margin-left: 0px;
    margin-top: -3%;
}
</style>
	<!-- UI X -->
		<div class="ui-208">
		
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-6 col-xs-12" >
					
						<!-- UI Item -->
						<div class="ui-item">
							<!-- Image -->
							<a href="#"><img src="img/ui-208/1.jpg" alt="" class="img-responsive" /></a>
							<!-- Content -->
							<div class="content">
								<!-- Heading -->
								<h3><a href="#">Lorem ipsum dolor amet</a></h3>
								<!-- Paragraph -->
								<p>Quisque nisl leo, blandit in magna vel, pulvinar vehicula augue. Phasellus nulla enim, sodales eget eleifend ut, rutrum et velit.</p>
								<!-- Button -->
								<a href="#" class="btn btn-red">Read More</a>
							</div>
						</div>
					
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
					
						<!-- UI Item -->
						<div class="ui-item">
							<!-- Image -->
							<a href="#"><img src="img/ui-208/2.jpg" alt="" class="img-responsive" /></a>
							<!-- Content -->
							<div class="content">
								<!-- Heading -->
								<h3><a href="#">Lorem ipsum dolor amet</a></h3>
								<!-- Paragraph -->
								<p>Quisque nisl leo, blandit in magna vel, pulvinar vehicula augue. Phasellus nulla enim, sodales eget eleifend ut, rutrum et velit.</p>
								<!-- Button -->
								<a href="#" class="btn btn-green">Read More</a>
							</div>
						</div>
					
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
					
						<!-- UI Item -->
						<div class="ui-item">
							<!-- Image -->
							<a href="#"><img src="img/ui-208/3.jpg" alt="" class="img-responsive" /></a>
							<!-- Content -->
							<div class="content">
								<!-- Heading -->
								<h3><a href="#">Lorem ipsum dolor amet</a></h3>
								<!-- Paragraph -->
								<p>Quisque nisl leo, blandit in magna vel, pulvinar vehicula augue. Phasellus nulla enim, sodales eget eleifend ut, rutrum et velit.</p>
								<!-- Button -->
								<a href="#" class="btn btn-lblue">Read More</a>
							</div>
						</div>
					
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12" style="margin-top: 2%;">
						
						
							 <form style="border:1px solid lightgray;">
							 		<h4 style="text-align:center;">Tell Us Your Buy Requirement</h4>
							    <div class="form-group">
							     <!--  <label for="email">Email:</label> -->
							     <div>
							      <input type="text" class="form-control" id="product_name" placeholder="Enter product name">
							    </div>
							</div>
							  <div class="form-group">
								
								  <textarea class="form-control" rows="5" id="details" placeholder="provide details like product specification,usage/application etc for best quotes."></textarea>
								</div>

								 <div class="form-group">
							     <!--  <label for="email">Email:</label> -->
							      <input type="text" class="form-control" id="mobile" placeholder="Enter Mobile No.">
							    </div>
							    <div class="form-group">
							     <!--  <label for="email">Email:</label> -->
							      <input type="email" class="form-control" id="email" placeholder="Enter Email">
							    </div>
							    <div class="form-group">
							     <!--  <label for="email">Email:</label> -->
							      <input type="text" class="form-control" id="city" placeholder="Enter City">
							    </div>
							    	<button type="button" class="btn btn-info btn-block">Send Your Requirement</button>
							    	<ul>
							    		<li>Save time in searching of product</li>
							    		<li>Get multiple price quotations</li>
							    		<li>Compare.evaluate and buy</li>
							    	</ul>	
							</form>

					

					</div>
					</div>
					<div class="row">
					<div class="col-md-3 col-sm-6 col-xs-12">
					
						<!-- UI Item -->
						<div class="ui-item">
							<!-- Image -->
							<a href="#"><img src="img/ui-208/4.jpg" alt="" class="img-responsive" /></a>
							<!-- Content -->
							<div class="content">
								<!-- Heading -->
								<h3><a href="#">Lorem ipsum dolor amet</a></h3>
								<!-- Paragraph -->
								<p>Quisque nisl leo, blandit in magna vel, pulvinar vehicula augue. Phasellus nulla enim, sodales eget eleifend ut, rutrum et velit.</p>
								<!-- Button -->
								<a href="#" class="btn btn-blue">Read More</a>
							</div>
						</div>
					
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
					
						<!-- UI Item -->
						<div class="ui-item">
							<!-- Image -->
							<a href="#"><img src="img/ui-208/5.jpg" alt="" class="img-responsive" /></a>
							<!-- Content -->
							<div class="content">
								<!-- Heading -->
								<h3><a href="#">Lorem ipsum dolor amet</a></h3>
								<!-- Paragraph -->
								<p>Quisque nisl leo, blandit in magna vel, pulvinar vehicula augue. Phasellus nulla enim, sodales eget eleifend ut, rutrum et velit.</p>
								<!-- Button -->
								<a href="#" class="btn btn-orange">Read More</a>
							</div>
						</div>
					
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
					
						<!-- UI Item -->
						<div class="ui-item">
							<!-- Image -->
							<a href="#"><img src="img/ui-208/6.jpg" alt="" class="img-responsive" /></a>
							<!-- Content -->
							<div class="content">
								<!-- Heading -->
								<h3><a href="#">Lorem ipsum dolor amet</a></h3>
								<!-- Paragraph -->
								<p>Quisque nisl leo, blandit in magna vel, pulvinar vehicula augue. Phasellus nulla enim, sodales eget eleifend ut, rutrum et velit.</p>
								<!-- Button -->
								<a href="#" class="btn btn-yellow">Read More</a>
							</div>
						</div>
					
					</div>

				</div>

				</div>
			</div>
		
	
		
		